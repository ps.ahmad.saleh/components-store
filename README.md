## ProgressSoft's Components Store

This store contains reusable components built by various PS teams

| Component       | Description                                            |Maintainer|Links|
|-----------------|--------------------------------------------------------|----------|------|
|ECC API Service|Provides a RESTful APIs for all the basic functions of ECC. It also exposes these functions asynchronously|Kryptonite|[ECC API Resources](#ecc-api-resources)|
|Users Preferences Service|A store for UI application user preferences.|Saturn|[User Preferences Resources](#user-preferences-resources)|
|Numbers Transcription|An API for converting numbers from numeric format to words|Kryptonite|[Numbers Transcription Resources](#numbers-transcription-resources)|
|Reconciliation|Dynamic financial transactions reconciliation engine that allows defining sources, templates and file types|Kryptonite|[Reconciliation Resources](#reconciliation-resources)|
|Entitlements||Kryptonite||
|Payments Templates||Kryptonite||
|System Configs||Kryptonite||
|Alerts Service||Kryptonite||

#### ECC API Resources
* [Gitlab Project](https://gitlab.com/progressoft/kryptonite/checks/ecc-api)
* [docker images on harbor](https://harbor.progressoft.io/harbor/projects/13/repositories/kryptonite%2Fecc-api)


#### User Preferences Resources
* [Gitlab Project](https://gitlab.com/progressoft/checks/saturn/micro-services/user-preferences-service)
* [docker images on harbor](https://harbor.progressoft.io/harbor/projects/33/repositories/saturn%2Fuser-preferences-service)


#### Numbers Transcription Resources
* [Gitlab Project](https://gitlab.com/progressoft/kryptonite/numbers-to-words)
* Maven Dependency:
    ```xml
    <dependency>
        <groupId>com.progressoft.corpay</groupId>
        <artifactId>numbers-to-words</artifactId>
        <version>1.8.RELEASE</version>
    </dependency>
    ```
    
#### Reconciliation Resources
* [Gitlab Project](https://gitlab.com/progressoft/kryptonite/reconciliation)
* [docker images on harbor](https://harbor.progressoft.io/harbor/projects/13/repositories/kryptonite%2Freconciliation)